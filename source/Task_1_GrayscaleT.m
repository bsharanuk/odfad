
vidobj = VideoReader('road_vid.mp4');

videoFileReader = vision.VideoFileReader('road_vid.mp4');
videoFrame = step(videoFileReader);
frameSize = size(videoFrame);
videoPlayer = vision.VideoPlayer('Position', [1 1 [frameSize(2), frameSize(1)]]);

roiMask = poly2mask([700 830 1170 100],[440 440 650 720],720,1280);

runLoop = true;
%frameCount = 1;

while runLoop && hasFrame(vidobj)
    
    videoFrame = readFrame(vidobj);
    %videoFrame = imcrop(videoFrame,[0,380,1280,720]);
    
    videoFrameGray = rgb2gray(videoFrame);
    %frameCount = frameCount + 1;
    
    videoFrameCanny = edge(videoFrameGray,'Canny',0.18);
    videoFrameROI = videoFrameCanny.* roiMask;
    test = bsxfun(@plus, imcomplement(videoFrameGray),  uint8(255*videoFrameROI));
    
   %Overlay Line
    
   [H,T,R] = hough(videoFrameROI);
   P  = houghpeaks(H,5,'threshold',ceil(0.1*max(H(:))));
   %x = T(P(:,2)); y = R(P(:,1));
   lines = houghlines(videoFrameROI,T,R,P,'FillGap',200,'MinLength',25);
   test1 = insertShape(videoFrame,'Line',[lines(1).point1 lines(1).point2],'LineWidth',5);
   for k = 2:length(lines)
    xy = [lines(k).point1; lines(k).point2];
    test1 = insertShape(test1,'Line',[lines(k).point1 lines(k).point2],'LineWidth',5);
   end
    
    %step(videoPlayer, videoFrame);
    %step(videoPlayer, videoFrameCanny);
    step(videoPlayer, test1);
    
    runLoop = isOpen(videoPlayer);
    
end

[H,T,R] = hough(videoFrameROI);
imshow(H,[],'XData',T,'YData',R,'InitialMagnification','fit');
xlabel('\theta'), ylabel('\rho');
axis on, axis normal, hold on;
P  = houghpeaks(H,10,'threshold',ceil(0.2*max(H(:))));
x = T(P(:,2)); y = R(P(:,1));
plot(x,y,'s','color','white');
lines = houghlines(videoFrameROI,T,R,P,'FillGap',100,'MinLength',80);
figure, imshow(videoFrame), hold on
for k = 1:length(lines)
xy = [lines(k).point1; lines(k).point2];
 plot(xy(:,1),xy(:,2),'LineWidth',2,'Color','green');
end